const std = @import("std");
const config = @import("config");
const File = std.fs.File;

pub const CursorPos = struct {
    row: u16,
    col: u16,
};

pub const Editor = struct {
    ally: std.mem.Allocator,
    term: Terminal,
    rows: u16,
    cols: u16,
    cursor: CursorPos,
    textbuf: std.ArrayList(u8),

    pub fn init(allocator: std.mem.Allocator, terminal: Terminal) !Editor {
        const size = terminal.size();
        return .{
            .ally = allocator,
            .term = terminal,
            .rows = size.row,
            .cols = size.col,
            .cursor = .{ .row = 0, .col = 0 },
            .textbuf = std.ArrayList(u8).init(allocator),
        };
    }

    pub fn deinit(self: Editor) void {
        self.textbuf.deinit();
    }

    pub fn refresh(self: *Editor) !void {
        try self.appendCommand(.{ .resetmode = .cursor });
        try self.appendCommand(.{ .setpos = .{} });

        try self.drawRows();

        try self.appendCommand(.{
            .setpos = .{
                .row = self.cursor.row + 1,
                .col = self.cursor.col + 1,
            },
        });
        try self.appendCommand(.{ .setmode = .cursor });

        self.term.write(self.textbuf.items);

        self.textbuf.items.len = 0;
    }

    pub fn clear(self: Editor) void {
        self.term.writeCommand(.{ .erase = .screen });
        self.term.writeCommand(.{ .setpos = .{} });
    }

    fn drawRows(self: *Editor) !void {
        for (0..self.rows) |row| {
            if (row == self.rows / 3) {
                const splash = std.fmt.comptimePrint("zed {}", .{config.version});
                const padding = (self.cols - splash.len) / 2;
                if (padding > 0) {
                    try self.textbuf.append('~');
                    try self.textbuf.appendNTimes(' ', padding - 1);
                }
                try self.textbuf.appendSlice(splash[0..@min(splash.len, self.cols)]);
            } else {
                try self.textbuf.append('~');
            }

            try self.appendCommand(.{ .eraseline = .right });

            if (row < self.rows - 1) {
                try self.textbuf.appendSlice("\r\n");
            }
        }
    }

    fn appendCommand(self: *Editor, command: Terminal.EscapeSequence) !void {
        try std.fmt.format(self.textbuf.writer(), "{}", .{command});
    }

    pub fn processKeypress(self: *Editor, key: u8) void {
        switch (key) {
            ctrl('q') => {
                self.clear();
                std.os.exit(0);
            },
            'h', 'j', 'k', 'l' => self.moveCursor(key),
            else => {},
        }
    }

    fn moveCursor(self: *Editor, key: u8) void {
        switch (key) {
            'h' => self.cursor.col -|= 1,
            'j' => self.cursor.row +|= 1,
            'k' => self.cursor.row -|= 1,
            'l' => self.cursor.col +|= 1,
            else => {},
        }
    }

    fn ctrl(key: u8) u8 {
        return key & 0x1f;
    }
};

pub const Terminal = struct {
    const os = std.os;
    const linux = os.linux;

    stdin: File,
    stdout: File,
    saved_ios: os.termios,

    pub fn init(in: File, out: File) Terminal {
        const ios = os.tcgetattr(in.handle) catch @panic("cannot read terminal attributes");
        os.tcsetattr(in.handle, os.TCSA.FLUSH, rawFlags(ios)) catch @panic("cannot set terminal raw mode");
        return .{
            .stdin = in,
            .stdout = out,
            .saved_ios = ios,
        };
    }

    pub fn deinit(self: Terminal) void {
        os.tcsetattr(self.stdin.handle, os.TCSA.FLUSH, self.saved_ios) catch
            @panic("failed to restore terminal attrs");
    }

    fn rawFlags(ios: os.termios) os.termios {
        var raw = ios;

        raw.iflag &= ~( // 'input' attributes:
            linux.BRKINT | // disable SIGINT from break conditions
            linux.ICRNL | // don't translate <C-m> ('\r') into '\n'
            linux.INPCK | // disable parity checking
            linux.ISTRIP | // don't strip the 8th bit of each byte
            linux.IXON // disable <C-s> and <C-q> control flow
        );
        raw.oflag &= ~( // 'output' attributes:
            linux.OPOST // don't translate '\n' into '\r\n'
        );
        raw.lflag &= ~( // 'local' attributes:
            linux.ECHO | // don't echo key press
            linux.ICANON | // read input byte-by-byte
            linux.IEXTEN | // disable <C-v> and <C-o> control characters
            linux.ISIG // disable <C-c> and <C-z> signals
        );
        raw.cflag |= linux.CS8; // set character size to 8 bits
        raw.cc[linux.V.MIN] = 0; // read input immediately
        raw.cc[linux.V.TIME] = 1; // wait 1/10 of a second (100ms) for input

        return raw;
    }

    pub fn write(self: Terminal, bytes: []const u8) void {
        const n = self.stdout.write(bytes) catch |err| {
            std.debug.panic("error {}\nwhilst writing bytes: {s}", .{ err, bytes });
        };
        std.debug.assert(n == bytes.len);
    }

    pub fn size(self: Terminal) CursorPos {
        var w: linux.winsize = undefined;
        const result = linux.ioctl(self.stdout.handle, linux.T.IOCGWINSZ, @intFromPtr(&w));
        if (result == -1) {
            self.writeCommand(.{ .cursor_right = 999 });
            self.writeCommand(.{ .cursor_down = 999 });
            return self.getCursorPos() catch |err|
                std.debug.panic("{}: failed to get cursor position", .{err});
        }

        std.debug.assert(w.ws_row > 0 and w.ws_col > 0);
        return .{ .row = w.ws_row, .col = w.ws_col };
    }

    fn writeCommand(self: Terminal, comptime command: EscapeSequence) void {
        self.write(std.fmt.comptimePrint("{}", .{command}));
    }

    pub fn getCursorPos(self: Terminal) !CursorPos {
        self.writeCommand(.{ .dsr = .position });

        var buf: [32]u8 = undefined;
        const coord_str = (try self.stdin.reader().readUntilDelimiter(&buf, 'R'))[2..];

        var result: [2]u16 = undefined;
        var it = std.mem.splitScalar(u8, coord_str, ';');
        var i: usize = 0;
        while (it.next()) |str| : (i += 1) {
            result[i] = try std.fmt.parseUnsigned(u16, str, 10);
        }

        return .{ .row = result[0], .col = result[1] };
    }

    pub fn readKey(self: *Terminal) !u8 {
        return self.stdin.reader().readByte();
    }

    // https://vt100.net/docs/vt100-ug/chapter3.html
    pub const EscapeSequence = union(enum) {
        pub const Feature = enum(u8) { cursor = 25 };

        dsr: enum(u3) { status = 5, position = 6 },
        setmode: Feature,
        resetmode: Feature,
        erase: enum { above, below, screen },
        eraseline: enum { right, left, all },
        setpos: struct { row: ?usize = null, col: ?usize = null },
        cursor_right: usize,
        cursor_down: usize,

        pub fn format(value: EscapeSequence, comptime _: []const u8, _: std.fmt.FormatOptions, writer: anytype) !void {
            const n = try writer.write("\x1b[");
            std.debug.assert(n == 2);

            try switch (value) {
                .dsr => |report| writer.print("{}n", .{@intFromEnum(report)}),
                .setmode => |mode| writer.print("?{}h", .{@intFromEnum(mode)}),
                .resetmode => |mode| writer.print("?{}l", .{@intFromEnum(mode)}),
                .erase => |area| writer.print("{}J", .{@intFromEnum(area)}),
                .eraseline => |area| writer.print("{}K", .{@intFromEnum(area)}),
                .setpos => |pos| writer.print("{};{}H", .{ pos.row orelse 0, pos.col orelse 0 }),
                .cursor_right => |cols| writer.print("{}C", .{cols}),
                .cursor_down => |rows| writer.print("{}B", .{rows}),
            };
        }
    };
};
